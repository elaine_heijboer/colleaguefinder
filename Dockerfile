# Dockerfile
FROM php:7.2-apache

RUN docker-php-ext-install pdo_mysql mysqli
RUN a2enmod rewrite

ADD . /var/www
ADD ./public /var/www/html

EXPOSE 80

WORKDIR /var/www
CMD php artisan migrate --force && apache2-foreground
