<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Desk;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Session\Session;

Route::get('/information', function () {
    return view('info');
});
Route::get('/id/{id}/continue', function($id){
        $desk = Desk::findOrFail($id);
        return view('home', compact('desk' ));
});

Auth::routes();
Route::get('autocomplete', 'SearchController@autocomplete')->name('autocomplete');
Route::get('/id/{id}', 'CheckinController@index');
Route::get('/', 'HomeController@index')->name('welcome');
Route::get('/plattegrond', 'PlattegrondController@index');
Route::patch('/id/{id}', 'CheckinController@update');
Route::any ('/search', 'SearchController@search')->name('search');
Route::any ('/floorplan/{id}', 'SearchController@floorplan');
Route::any ('/availability/{user}/{id}', 'CheckinController@availability');
Route::any ('/search/{id}', 'SearchController@searchid');
//authentication
Route::get('/signin', 'AuthController@signin');
Route::get('/authorize', 'AuthController@gettoken');
Route::get('/logout', 'AuthController@logout');
Route::get('/checkin', 'OutlookController@mail')->name('checkin');
Route::get('/checkout/{desk_id}', 'CheckinController@checkout');



