@extends('layouts.app')
@section('content')
<div class="col-sm-12 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4 search">
    @if(session()->has('popup'))
        <div class="popup-background">
            <div class="popup col-sm-12 col-lg-6 col-xl-4">
                <span class="popup-close glyphicon glyphicon-remove" aria-hidden="true"></span>
                <img src="{{asset('images/succes.gif')}}">
                <p>Status changed</p>
            </div>
        </div>
    @endif
    @if(Auth::check())
        <div class="searchfields" id="searchfields">
            <div class="purple-card-search">
                <h2 data-toggle="collapse" data-target="#collapseOne">Looking for someone?</h2>
                <div id="collapseOne" class="collapse collapsed" data-parent="#searchfields">
                    @include('includes.search-users')
                </div>
            </div>
            <div class="purple-card-search">
                <h2 data-toggle="collapse" data-target="#collapseTwo">Looking for an empty desk?</h2>
                <div id="collapseTwo" class="collapse collapsed" data-parent="#searchfields">
                @include('includes.search-desks')
                </div>
            </div>
        @if(isset($data['desk']))
            <div class="purple-card-search">
                <h2 data-toggle="collapse" data-target="#collapseThree">Change your availability</h2>
                <div id="collapseThree" class="collapse collapsed" data-parent="#searchfields">
                    @include('includes.change-status')
                </div>
            </div>
           @endif
        </div>
    @else
        <div class="purple-card">
            <p>Sign in to search for a colleague <br> or to find an empty desk.</p>
            @php
                $redirect = '/';
            @endphp
            @include('includes.login')
        </div>
    @endif
</div>
@endsection
