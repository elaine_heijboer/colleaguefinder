@php if(isset($desk)){ @endphp
<form action="/checkout" method="POST" role="search" class="checkout-button">
    {{ csrf_field() }}
    <label for="select">Checkout from desk {{$desk->desk_nr}}</label>
    <div class="form-group checkout-button">
        {!! Form::submit('Checkout', null, ['class'=>'btn btn-default']) !!}
    </div>
</form>
@php
    }
@endphp
