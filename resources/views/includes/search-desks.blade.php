<form action="/floorplan" method="POST" role="search">
        {{ csrf_field() }}
        <div class="searchbar">
            <select onchange="window.location = '/floorplan/' + this.options[this.selectedIndex].value;">
                @if(!isset($data['floor']))
                    <option value="">Select a floor...</option>
                @endif
                @if(isset($data))
                    @foreach($data['floors'] as $floor)
                        @php
                        if($floor->floor == 1){
                            $extentie = 'st';
                        }elseif($floor->floor == 2){
                            $extentie = 'nd';
                        }elseif($floor->floor == 3){
                            $extentie = 'rd';
                        }else{
                            $extentie = 'th';
                        }
                        @endphp
                        <option value="{{$floor->id}}" {{isset($data['floor']) && $data['floor']->id === $floor->id ? 'selected' : ''}}>{{$floor->floor}}{{$extentie}} Floor ({{$floor->free_desks}} empty desks) </option>
                    @endforeach
                @endif
            </select>
        </div>
 </form>
