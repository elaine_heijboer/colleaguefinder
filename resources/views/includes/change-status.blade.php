@php
if(Auth::check()){
$user = Auth::user();
}
@endphp

<form action="/status" method="POST" role="search">
    {{ csrf_field() }}
    <div class="searchbar">
        <select onchange="window.location = '/availability/{{$user->id}}/' + this.options[this.selectedIndex].value;">
            <option value="0" {{$user->status === 0 ? 'selected' : ''}}>Available</option>
            <option value="1" {{$user->status === 1 ? 'selected' : ''}}>Busy</option>
            <option value="2" {{$user->status === 2 ? 'selected' : ''}}>Hidden</option>
        </select>
    </div>
</form>
