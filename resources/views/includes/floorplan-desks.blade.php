@if(isset($data))
    @php
        $current_date = $data['current_date'];
        $floor_nr = $data['floor']->floor;

        if($floor_nr == 1){
            $extentie = 'st';
        }elseif($floor_nr == 2){
            $extentie = 'nd';
        }elseif($floor_nr == 3){
            $extentie = 'rd';
        }else{
            $extentie = 'th';
        }

        $freedesks = count($data['all-desks']) - count($data['occupied-desks']);
    @endphp
    <p><b>{{$freedesks}}</b> of the <b>{{count($data['all-desks'])}}</b> desks are available<br> at the {{$floor_nr}}{{$extentie}} floor.</p>
    @include('includes.floorplan-legend-floor')
    <div class="map">
        <div class="zoom-buttons">
            <div class="zoom-in"><span class="glyphicon glyphicon-zoom-in" onclick="zoomin()" aria-hidden="true"></span></div>
            <div class="zoom-out"><span class="glyphicon glyphicon-zoom-out" onclick="zoomout()" aria-hidden="true"></span></div>
        </div>
        <div class="map-overflow">
    <object id="map" data="{{asset('images/'. $data['floor']->image)}}" type="image/svg+xml"> </object>
        </div>
    </div>
    <p><b>{{$floor_nr}}{{$extentie}} Floor</b></p>
    <script>
        window.addEventListener("load", function() {
            var svgObject = document.getElementById('map').contentDocument;
            @foreach($data['all-desks'] as $desk)
                @if($desk->status === 1 && $desk->expiry_date > $current_date)
                    svgObject.getElementById('{!! $desk->id !!}').style.fill = "#ffbec3";
                @elseif($desk->screen_type === 1)
                    svgObject.getElementById('{!! $desk->id !!}').style.fill = "rgb(212, 240, 255)";
                @else
                    svgObject.getElementById('{!! $desk->id !!}').style.fill = "#FFF";
        @endif
            @endforeach
        });
    </script>
@endif
