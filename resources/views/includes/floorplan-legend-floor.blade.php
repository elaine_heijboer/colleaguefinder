<div class="legend" id="legendAccordion">
    <div class="legend-toggle" data-toggle="collapse" data-target="#legendboxes" aria-expanded="true" aria-controls="collapseOne">
        <h3>Legend</h3><img class="legend-arrow" src="{{asset('images/arrow.svg')}}">
    </div>

    <div id="legendboxes" class="collapse show" aria-labelledby="headingOne" data-parent="#legendAccordion">
        <div class="legend-parts">
            <div class="legend-box">
                <div class="box2"></div>
                <p>Empty desks</p>
            </div>
            <div class="legend-box">
                <div class="box3"></div>
                <p>Design screens</p>
            </div>
            <div class="legend-box">
                <div class="box4"></div>
                <p>Occupied desks</p>
            </div>
        </div>
    </div>
</div>
