@if(isset($data))
    @php
    //dd($data);
         $current_date = $data['current_date'];
         $floor_nr = $data['floor']->floor;
    @endphp

    @include('includes.floorplan-legend-users')
    <div class="map">
        <div class="zoom-buttons">
            <div class="zoom-in"><span class="glyphicon glyphicon-zoom-in" onclick="zoomin()" aria-hidden="true"></span></div>
            <div class="zoom-out"><span class="glyphicon glyphicon-zoom-out" onclick="zoomout()" aria-hidden="true"></span></div>
        </div>
        <div class="map-overflow">
            <object id="map" data="{{asset('images/'. $data['floor']->image)}}" type="image/svg+xml"> </object>
        </div>
    </div>
    <form class="checkbox">
        <input type="checkbox" onclick="myCheck()" id="checkbox"><p class="checkbox-text">Show occupied desks</p>
    </form>
    <script type="text/javascript">
            function myCheck() {
                var svgObject = document.getElementById('map').contentDocument;
                var checkBox = document.getElementById("checkbox");
                if (checkBox.checked === true) {
                    @foreach($data['all-desks'] as $id)
                        @if($id->status === 1 && $id->expiry_date > $current_date)
                            svgObject.getElementById('{!! $id->id !!}').style.fill = "#ffbec3";
                        @elseif($id->screen_type === 1)
                            svgObject.getElementById('{!! $id->id !!}').style.fill = "rgb(212, 240, 255)";
                        @else
                            svgObject.getElementById('{!! $id->id !!}').style.fill = "#FFF";
                        @endif
                    @endforeach
                    svgObject.getElementById('{!! $data['desk']->id !!}').style.fill = "#2f00be";
                } else {
                    @foreach($data['all-desks'] as $id)
                        svgObject.getElementById('{!! $id->id !!}').style.fill = "white";
                    @endforeach

                    @foreach($data['desks'] as $id)
                        svgObject.getElementById('{!! $id->id !!}').style.fill = "white";
                    @endforeach
                        svgObject.getElementById('{!! $data['desk']->id !!}').style.fill = "#2f00be";
                }
            }
            window.addEventListener("load", function() {
                var svgObject = document.getElementById('map').contentDocument;
                svgObject.getElementById('{!! $data['desk']->id !!}').style.fill = "#2f00be";
            });
    </script>
@endif
