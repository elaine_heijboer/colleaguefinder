@php
if(isset($query)){
    if($query != ''){
        $pholder = ucwords($query);
    }else{
        $pholder  = '';
    }
}else{
$pholder = '';
}
@endphp


<form action="/search" method="POST" role="search">
    {{ csrf_field() }}
    <div class="searchbar">
        <input type="text" class="typeahead form-control search-field" autocomplete="off" name="q" value="{{$pholder}}" placeholder="John Doe">
        <span class="input-group search-button">
            <button type="submit" class="btn btn-default">
                <span class="glyphicon glyphicon-search"></span>
            </button>
        </span>
    </div>
</form>

<script type="text/javascript">
    var path = "{{ route('autocomplete') }}";
    $('input.typeahead').typeahead({
        source:  function (query, process) {
            return $.get(path, { query: query }, function (data) {
                return process(data);
            });
        }
    });
</script>
