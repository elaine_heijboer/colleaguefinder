@extends('layouts.app')
@section('content')
    <div class="container-fluid home">
        <div class="row justify-content-center">
            <div class="col-sm-12 col-lg-6 col-lg-6 col-xl-4">
                @if(Auth::check())
                   @php
                    $user_id = Auth::user()->id;
                    $user = Auth::user();
                    @endphp
                <div class="purple-card">
                     <p class="checkin">Your about to check in<br> to desk nr <b>{{$desk->desk_nr}}.</b> You can<br>change your availability here.</p>

                   <b class="label" >Availability</b>
                    {!! Form::model($desk, ['method'=>'PATCH', 'action'=>['CheckinController@update', $desk->id]]) !!}

                    {{-- hidden user_id input --}}
                    <div class="form-group">
                        {!! Form::hidden('user_id', $user_id, ['class'=>'form-control']) !!}
                    </div>

                   {{-- hidden expiry date input --}}
                       <div class="form-group">
                           {!! Form::hidden('expiry_date', old('expiry_date', Carbon\Carbon::today()->format('Y-m-d 23:56:00')), ['class'=>'form-control']) !!}
                       </div>
                        {{-- radio button --}}
                        <div class="form-group status">
                            <div class="status-option">
                                {!! Form::radio('status', 0, ['class'=>'form-control checkmark']) !!} <p>Available</p>
                            </div>
                            <div class="status-option">
                            {!! Form::radio('status', 1, ['class'=>'form-control checkmark']) !!} <p>Busy</p>
                            </div>
                            <div class="status-option">
                                {!! Form::radio('status', 2, ['class'=>'form-control checkmark']) !!} <p>Hidden</p>
                            </div>
                        </div>

                        {{--Submit button--}}
                        <div class="form-group buttons">
                            <a href="/">Cancel</a>
                            {!! Form::submit('Checkin', null, ['class'=>'btn btn-default']) !!}
                        </div>

                        {!! Form::close() !!}
                </div>
                @else
                    <div class="purple-card">
                        <p>Do you want to check-in<br> at desk nr <b>{{$desk->desk_nr}}</b>?</p>
                        @php
                            $redirect = '/id/'.$desk->id;
                        @endphp
                        @include('includes.login')
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
