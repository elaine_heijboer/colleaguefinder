@extends('layouts.app')
@section('content')
    <div class="container-fluid home">
        <div class="row justify-content-center">
            <div class="col-sm-12 col-lg-6 col-xl-4">
                @if(Auth::check())
                    <div class="white-card">
                        <p>Someone else is already checked in.<br>
                        What do you want to do?</p>
                        <div class="white-card-buttons">
                            <a href="/">Cancel</a>
                            <a href="/id/{{$desk->id}}/continue">Check-in</a>
                        </div>
                    </div>
                @else
                    <div class="purple-card">
                        <p>Do you want to check-in<br> at desk nr <b>{{$desk->desk_nr}}</b>?</p>
                        @php
                            $redirect = '/id/'.$desk->id;
                        @endphp
                        @include('includes.login')
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
