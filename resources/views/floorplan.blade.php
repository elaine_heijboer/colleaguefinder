@extends('layouts.app')
@section('content')
    <div class="container-fluid search">
        <div class="row">
            <div class="col-sm-12 col-lg-6 offset-lg-3 col-xl-6 offset-xl-3">
                @if(Auth::check())
                    <?php $user_id = Auth::user()->id; $user = Auth::user(); ?>
                <div class="purple-card-search">
                    <h2>Looking for an empty desk?</h2>
                    @include('includes.search-desks')
                    @include('includes.floorplan-desks')
                @else
                    @include('includes.login')
                @endif
                </div>
            </div>
        </div>
    </div>
@endsection
