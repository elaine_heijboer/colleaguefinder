@extends('layouts.app')
@section('content')
    <div class="container-fluid information">
        <div class="row" style="flex-direction: column;">
            <div class="col-sm-12 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
                <h1 style="margin: 10px 0; height:auto">Information</h1>
                <br>
                <h2>Privacy</h2>
                <p> Did you know we only save your Greenhouse
                    office 365 name, email
                    and id? We don’t save any data regarding your
                    Incheck time. When the workday is
                    over the database will automatically make your checkin invalid.</p>
                <p>
                    The only data we use are your Greenhouse office 365 name, email and Id to make
                    sure your colleagues can find you.
                </p>
                <p>
                    After you sign in with Greenhouse office 365 you get the option
                    to set your status. If you don’t want colleagues
                    to be able to search for you, you can
                    set your status on “Hidden”. This way we know the desk is
                    in use. </p>

                <h2>Why does Colleague finder exist?</h2>
                <p>Colleague finder is a tool to search for colleagues and to find empty desks at location Eindhoven.
                    It is essential that a large part of the (flexplek)workers check in so that the data is reliable.
                    Even if you don't want to be found you can checkin and set your status on hidden. This way the system
                    knows that the desk is not available.
                </p>
                <br>
                <h2>How to checkin?</h2>
                <p>
                    You can checkin at your desk by scanning the NFC tag with your phone,
                    to do this you might need to enable NFC on your phone.

                    If your phone doesn't support NFC you can scan the QR-code. Older phones
                    might need a app to scan QR-codes.
                </p>
                <br>
               <h2>How to checkout?</h2>
                <p>
                    If you leave after 15:00 you don't need to check out. The system makes sure
                    your checkin is invalid after 23:00 the same day.

                    If you leave before 15:00 we kindly ask you to checkout this because your colleagues
                    can see that the desk is empty and that you are not on this location anymore.

                    Your checkin or checkout time WON'T be saved.

                    There are two ways to checkout,
                </p>
                <br>
                <h2>How to search for a colleague?</h2>
                <p>You will need to login with your greenhouse office 365 account. After you login you get a set of input fields
                at the first field you can search for your colleague's name and lastname.
                </p>
                <br>
                <h2>How to search for an empty desk?</h2>
                <p>You will need to login with your greenhouse office 365 account. After you login you get a set of input fields
                    at the second field you can choose the floor where you want to sit. You'll get a Floorplan with the currently in used desks colored Red.
                    you can also see how many desks are still available.
                </p>
            </div>
        </div>
    </div>
@endsection
