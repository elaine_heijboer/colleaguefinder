@extends('layouts.app')
@section('content')
    <div class="container-fluid search">
        <div class="row">
            <div class="col-sm-12 col-lg-6 offset-lg-3 col-xl-6 offset-xl-3 search__info">
{{--            check if user is logged in    --}}
            @if(Auth::check())
                @php $user_id = Auth::user()->id; $user = Auth::user(); @endphp
                <div class="purple-card-search">
                    <h2>Looking for someone?</h2>
                    @include('includes.search-users')
                    @if (isset($message))
                        <p>{{$message}}</p>
                    @endif
            @if(isset($details))
                @php $current_date = $details['current_date']; @endphp
                <p> There are multiple results, <br> which <b> {{ ucwords($query) }} </b> are you looking for?</p>

                @foreach($details['users'] as $user)
                    @if(($user->desk != null))
                        @php
                            if($user->status === 0 && $user->desk->expiry_date > $current_date){
                                $status = 'Available';
                            }elseif($user->status === 1 && $user->desk->expiry_date > $current_date){
                                $status = 'Busy';
                            }else{
                                $status = 'Not checked in';
                            }
                        @endphp
                            @if($user->status === null || $user->status === 2 || $user->desk->expiry_date < $current_date)
                                <div class="users-info disabled">
                                    <p><b>{{$user->name}}</b>, {{$user->jobtitle}}</p>
                                    <i>Not checked in</i>
                                </div>
                            @else
                                <a href="/search/{{$user->id}}" class="users-info">
                                    <p><b>{{$user->name}}</b>, {{$user->jobtitle}}</p>
                                    <i>{{$status}}</i>
                                </a>
                            @endif
                    @else
                        <div class="users-info disabled">
                            <p><b>{{$user->name}}</b>, {{$user->jobtitle}}</p>
                            <i>Not checked in</i>
                        </div>
                    @endif
                @endforeach
            @endif
            @if(isset($data))
                @if(isset($data['desk']))
                     @php
                     $desk_nr = $data['desk']->desk_nr;
                     $floor_nr = substr($desk_nr, 1, 1);

                     if($floor_nr == 1){
                         $extentie = 'st';
                     }elseif($floor_nr == 2){
                         $extentie = 'nd';
                     }elseif($floor_nr == 3){
                         $extentie = 'rd';
                     }else{
                         $extentie = 'th';
                     }
                     @endphp

                     @if(($data['desk'] != null))
                            @if($data['user']->status === 0 && $data['desk']->expiry_date > $data['current_date'])
                                <p class="search__result light"><b>{{$data['user']->name}}</b> is at the {{$floor_nr}}{{$extentie}} floor <br> at desk <b>{{$desk_nr}}</b> </p>
                                    @include('includes.floorplan-users')
                                <p><b>{{$floor_nr}}{{$extentie}} Floor</b></p>
                            @elseif($data['user']->status === 1 && $data['desk']->expiry_date > $data['current_date'])
                                <p class="search__result"><b>{{$data['user']->name}}</b> is currently busy please only visit him/her if it is urgent, else please send a message.</p>
                                <p class="search__result light"><b>{{$data['user']->name}}</b> is at the {{$floor_nr}}{{$extentie}} floor at desk <b>{{$desk_nr}}</b></p>
                                @include('includes.floorplan-users')
                                <p><b>{{$floor_nr}}{{$extentie}} Floor</b></p>
                            @else
                                <p class="search__result light"><b>{{$data['user']->name}}</b> is not checked in.</p>
                            @endif
                     @else
                         <p class="search__result light"><b>{{$data['user']->name}}</b> is not checked in.</p>
                     @endif
                @endif
            @endif
            </div>
{{--            if user is not logged in show login screen  --}}
            @else
                @include('includes.login')
            @endif
            </div>
        </div>
    </div>
@endsection
