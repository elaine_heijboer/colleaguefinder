@extends('layouts.app')

@section('content')
    <div class="container-fluid home">
        <div class="row justify-content-center">
            <div class="col-md-12 error-page">
                <img class="parrot" src="{{asset('images/sadparrot.gif')}}" />
                <h1>Not the desk you're looking for.</h1>
                <p>Please scan the tag again</p>
            </div>
        </div>
    </div>
@endsection
