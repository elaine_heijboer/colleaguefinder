@if(Auth::check())
    @php
    $user = Auth::user();
    $user_name = $user->name;

    @endphp
<footer class="row">
    <div class="col-md-12 footer">
        <a href="/logout" role="button" id="logout">
            <span class="glyphicon glyphicon-off"></span>
            <p>Sign out</p>
        </a>
        @if (\Route::current()->getName() == 'welcome')
        @if(isset($data['desk']))
            @php $desk_id = $data['desk']->id; @endphp
        <a href="/checkout/{{$desk_id}}" role="button" id="logout" class="checkout">
            <span class="glyphicon glyphicon-log-out"></span>

            <p>Checkout desk <b>{{$data['desk']->desk_nr}}</b></p>
        </a>
        @endif
    @endif
    </div>
</footer>
@endif
