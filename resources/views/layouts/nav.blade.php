@php
    if(Auth::check()){
       $user = Auth::user();
       $user_name = $user->name;
    }else{
       $user_name = '';
    }
@endphp


<div class="row">
    <nav class="col-md-12">
        <a href="/"><img class="logo" src="{{asset('images/logo-whitev2.png')}}" /></a>
        <div class="info">
            <a href="/information">Privacy</a>
            <a href="/information"><img class="infoicon" src="{{asset('images/information.svg')}}" /></a>
        </div>
    </nav>
</div>
@if(!\Request::is('information'))
    <div class="row">
        <div class="col-md-12">
            <h1>Goodmorning!<br>
                {{$user_name}}
            </h1>
        </div>
    </div>
@endif


