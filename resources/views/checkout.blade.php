<?php
$dir = 'images/parrots/';
$images_array = glob($dir.'*.gif');
$random_array = array_rand($images_array);
$random_parrot = $images_array[$random_array];
?>

@extends('layouts.app')
@section('content')
    <div class="container thanks">
        <div class="row justify-content-center">
            <div class="col-sm-12">
                @if(Auth::check())
                    <?php
                    $user_id = Auth::user()->id;
                    $user = Auth::user();
                    ?>
                    @endif
                <div class="card">
                    <span class="close glyphicon glyphicon-remove" onclick="window.location.href='/'" aria-hidden="true"></span>
                    <img class="parrot" src="{{asset($random_parrot)}}" />
                    <b>Thank you for checking out</b>
                    <p>You're now checked out of desk <b>{{$desk->desk_nr}}</b> to check-in scan the NFC tag again.</p>
                    <p>You can now close this page and have a nice day!</p>
                </div>
            </div>
        </div>
    </div>
@endsection
