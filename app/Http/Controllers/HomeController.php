<?php
namespace App\Http\Controllers;

use App\Desk;
use App\Floorplan;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
   public function index(){
       if(auth::check()){
           $user = Auth::user();
           $data['desk'] = Desk::where('user_id', '=', $user->id)->first();
           $data['floors'] = [];
           //set count of empty desks at the optionfield "floor"
           $current_date = Carbon::now()->toDateTimeString();
           $floors = Floorplan::get();
           foreach($floors as $floor){
               $all_desks = Desk::where('floor_nr', '=', $floor->floor)->count();
               $desks = Desk::where('status', '=', 1)->where('floor_nr', '=', $floor->floor)->where('expiry_date', '>', $current_date)->count();
               $floor['free_desks'] = $all_desks - $desks;
               $data['floors'][] = $floor;
           }
           return view('welcome')->withData($data);
       }else{
           return view('welcome');
       }
   }
}
