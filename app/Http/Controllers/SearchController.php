<?php

namespace App\Http\Controllers;

use App\Desk;
use App\User;
use App\Floorplan;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class SearchController extends Controller
{
    public function autocomplete(Request $request){
        $data = User::select("name")->where("name","LIKE","%{$request->input('query')}%")->get();
        return response()->json($data);
    }

    public function search()
    {
        //get the search question
        $q = Input::get('q');
        if (strlen($q) > 1){
            //get the usere where name or email has the search question
            $details['results'] = User::where('name', 'LIKE', '%' . $q . '%')->orWhere('email', 'LIKE', '%' . $q . '%')->get();
            //if result is not 1 get list of all users where the search question matches
            if (count($details['results']) > 1){
                $details['users'] = [];
                $details['current_date'] = Carbon::now()->toDateTimeString();
                $users = $details['results'];
                foreach($users as $user){
                    $user['desk'] = Desk::where('user_id', '=', $user->id)->first();
                    $details['users'][] = $user;
                }
                return view('search')->withDetails($details)->withQuery($q);
            }
            //return the welcome view with the users data and the search question
            elseif (count($details['results']) === 0){
                return view( 'search')->withMessage('no users found')->withQuery($q);
            }
            //if result is 1
            else {
                //get the first result
                $data['user'] = $details['results'][0];
                $data['current_date'] = Carbon::now()->toDateTimeString();
                $data['desk'] = Desk::where('user_id', '=', $data['user']->id)->first();
                if($data['desk'] != null){
                    $data['floor'] = Floorplan::where('floor', '=', $data['desk']->floor_nr)->first();
                    $data['all-desks'] = Desk::where('floor_nr', '=', $data['floor']->floor)->get();
                    $data['desks'] = Desk::where('status', '=', 1)->where('floor_nr', '=', $data['floor']->floor)->where('expiry_date', '>', $data['current_date'])->get();
                }else{
                    return view( 'search')->withMessage('User not checked-in')->withQuery($q);
                }
                //find the desk where the user_id matches the first result
                $desk = Desk::where('user_id', '=', $data['user']->id)->first();
                if(count($desk) != null){
                    $data['desk'] = $desk;
                }else{
                    return view( 'search')->withMessage('User not checked-in')->withQuery($q);
                }
                //return view welcome with the user, desk and search question
                return view('search')->withData($data)->withQuery($q);
            }
        }else{
           //Controller code
           return view( 'search')->withMessage('Search to short')->withQuery($q);
        }
    }
    public function searchid($id)
    {
        $data['current_date'] = Carbon::now();
        $data['user'] = User::FindOrFail($id);
        $data['desk'] = Desk::where('user_id', '=', $id)->first();
        $data['floor'] = Floorplan::where('floor', '=', $data['desk']->floor_nr)->first();
        $data['all-desks'] = Desk::where('floor_nr', '=', $data['floor']->floor)->get();
        $data['desks'] = Desk::where('status', '=', 1)->where('floor_nr', '=', $data['floor']->floor)->where('expiry_date', '>', $data['current_date'])->get();
        return view('search')->withData($data);
    }

    public function floorplan($id)
    {
        $data['current_date'] = Carbon::now()->toDateTimeString();
        $data['floor'] = Floorplan::FindOrFail($id);
        $data['all-desks'] = Desk::where('floor_nr', '=', $data['floor']->floor)->get();
        $data['occupied-desks'] = Desk::where('status', '=', 1)->where('floor_nr', '=', $data['floor']->floor)->where('expiry_date', '>', $data['current_date'])->get();
        $data['floors'] = [];

        $floors = Floorplan::get();
        foreach($floors as $floor){
            $all_desks = Desk::where('floor_nr', '=', $floor->floor)->count();
            $occupied_desks = Desk::where('status', '=', 1)->where('floor_nr', '=', $floor->floor)->where('expiry_date', '>', $data['current_date'])->count();
            $floor['free_desks'] = $all_desks - $occupied_desks;
            $data['floors'][] = $floor;
        }
        return view('floorplan')->withData($data);
    }
}
