<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Microsoft\Graph\Graph;
use Microsoft\Graph\Model;

class OutlookController extends Controller
{
    public function mail()
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }

        $tokenCache = new \App\TokenStore\TokenCache;

        $graph = new Graph();
        $graph->setAccessToken($tokenCache->getAccessToken());

        $user = $graph->createRequest('GET', '/me')
            ->setReturnType(Model\User::class)
            ->execute();

        //echo 'User: '.$user->getDisplayName() . ' ('. $user->getMail() . ' ' .$user->getSurName() . ') - '.$user->getId();
        $user_id = $user->getId();
        $name = $user->getGivenName() .' '. $user->getSurName();
        $mail = $user->getUserPrincipalName();
        $jobtitle = $user->getjobTitle();

        $user_save = User::where("office_id", $user_id)->first();
        if($user_save == null){
            $user_save = new User();
            $user_save->office_id = $user_id;
        }
        $user_save->name = $name;
        $user_save->email = $mail;
        $user_save->jobtitle = $jobtitle;
        $user_save->save();
        Auth::login($user_save);
        return redirect(isset($_SESSION['redirect']) ? $_SESSION['redirect'] : '/');

    }
}
