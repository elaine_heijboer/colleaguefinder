<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Desk;
use App\User;
use Illuminate\Support\Facades\Auth;

class CheckinController extends Controller
{
    public function index($id, \GuzzleHttp\Client $httpClient)
    {
        $desk = Desk::findOrFail($id);
        if (Auth::check()) {
            $user = Auth::user();
            $current_time = Carbon::now();
            if ($desk->user_id != $user->id && $desk->expiry_date > $current_time) {
                return view('checkincheck', compact('desk'));
            }else{
                return view('home', compact('desk'));
            }
        } else {
            return view('home', compact('desk'));
        }
    }

    public function update(Request $request, $id)
    {

        // get user id
        $user_id = $request->input('user_id');
        // find user with user id
        $user = User::FindOrFail($user_id);
        // find if user is already checked in
        $old_desk = Desk::where('user_id', '=', $user->id)->first();

        //if user is checked in set status, user_id and expiry_date to null
        if(isset($old_desk->expiry_date)){
            $old_desk->update(['status' => null]);
            $old_desk->update(['user_id' => null]);
            $old_desk->update(['expiry_date' => null]);
        }

        //find desk where id = id from url
        $desk = Desk::FindOrFail($id);
        $input = $request->all();
        //update database
        $user->update($input);
        $desk->update($input);
        $desk->update(['status' => 1]);
        return view('thanks', compact('desk'));
    }

    public function checkout($desk_id){
        $desk = Desk::FindOrFail($desk_id);
        $desk->update(['status' => null]);
        $desk->update(['user_id' => null]);
        $desk->update(['expiry_date' => null]);
        return view('checkout', compact('desk'));
    }

    public function availability($user, $id){
        $user = User::findOrFail($user);
        $user->update(['status' => $id]);
        return redirect('/')->with('popup', 'thanks');
    }
}
