<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Floorplan extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'id', 'floor', 'image'
    ];

}
