<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Desk extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'desk_nr', 'status', 'user_id', 'expiry_date'
    ];

    public function user()
    {
        return $this->hasOne('App\User');
    }

}
