//privacy bar open-close
$('.toggle').click(function(){
    if($('.collapse').hasClass('show')){
        $('.transform').html('<p>Did you know we only save your office name, email and id?</p>');
        $('.open').css('transform', 'rotate(0deg)');
    }else{
        $('.transform').html('<h2>Privacy</h2>');
        $('.open').css('transform', 'rotate(180deg)');
    }
});

//turn arrow legend
$('.legend-toggle').click(function(){
    if( $('.collapse').hasClass('show')){
        $('.legend-arrow').css('transform', 'rotate(0deg)');
    }else{
        $('.legend-arrow').css('transform', 'rotate(180deg)');
    }
});


//change status popup
$(function() {
    $('.popup-background').delay(1500).fadeOut();
});

$('.popup-close').click(function(){
    $('.popup-background').hide();
});

function zoomin(){
    var zoom = document.getElementById("map");
    var currWidth = zoom.clientWidth;
    zoom.style.width = (currWidth + 200) + "px";
}
function zoomout(){
    var zoom = document.getElementById("map");
    var currWidth = zoom.clientWidth;
    zoom.style.width = (currWidth - 200) + "px";
}
